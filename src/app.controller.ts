import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/status')
  getStatus(): string {
    return this.appService.getHello();
  }

  @Get('/city')
  getCity(): string[] {
    const cities = [
      'Paris',
      'Bordeaux',
      'Lyon',
      'Strasbourg',
      'Toulouse',
      'Marseille',
    ];
    return cities;
  }
}
